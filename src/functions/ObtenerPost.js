import axios from "axios";
import { useState } from "react";

// PETICIÓN GET
const ObtenerPost = async () => {
  const [users, setUsers] = useState();

  try {
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    console.log(data);
    setUsers(data);
    console.log(users)
  } catch (err) {
    console.error(err);
  }
};
export default ObtenerPost;
