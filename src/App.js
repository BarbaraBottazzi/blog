import Login from "./components/login";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/home";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import ProtectedRoute from "./protectedRoute";
import { useEffect } from "react";
import { Redirect } from "react-router";
import Add from "./components/add";
import Edit from "./components/edit";
import Heading from "./components/Heading";

function App() {
  return (
    <Router>
      <Route exact path="/" component={Login} />
      <ProtectedRoute exact path="/edit" component={Edit} />
      <ProtectedRoute exact path="/add" component={Add} />
      <ProtectedRoute exact path="/Home" component={Home} />
    </Router>
  );
}

export default App;
