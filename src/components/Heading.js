import React from "react";
import { Link } from "react-router-dom";
import EliminarLocalStorage from "../functions/eliminarLocalStorage";

function Heading() {
  return (
    <React.Fragment>
      <div className="container-fluid">
        <div className="row">
          <div className=" bg-white p-3 col-12 text-center">
            <div className="row ">
              <div className="col-4 font-weight-bold">CRUD</div>

              <div className="col-4"></div>
              <div className="col-4 ">
                <Link
                  style={{ textDecoration: "none", color: "black" }}
                  className=" ml-2 px-3  border-left"
                  to="/edit"
                >
                  Edit{" "}
                </Link>
                <Link
                  className=" ml-2 px-3  border-left"
                  to="/add"
                  style={{ textDecoration: "none", color: "black" }}
                >
                  Add{" "}
                </Link>
                <Link
                  style={{ textDecoration: "none", color: "black" }}
                  onClick={() => EliminarLocalStorage()}
                  className=" ml-2 px-3  border-left"
                  to="/"
                >
                  {" "}
                  Logout
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Heading;
