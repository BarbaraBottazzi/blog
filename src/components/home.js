import axios from "axios";
import React, { useState, useMemo } from "react";
import { useEffect } from "react";
import Add from "./add";
import Edit from "./edit";
import TitleList from "./TitleList";
import { withRouter } from "react-router";
import Heading from "./Heading";

function Home() {
  const [newPost, setNewPost] = useState();
  const [id, setId] = useState();
  const [detalles, setDetalles] = useState(null);
  const [users, setUsers] = useState();

  console.log(users, "contusers");

  // PETICIÓN GET

  const ObtenerPost = async () => {
    try {
      const { data } = await axios.get(
        "https://jsonplaceholder.typicode.com/posts"
      );
      console.log(data);
      setUsers(data);
    } catch (err) {
      console.error(err);
    }
  };
  useEffect(() => {
    ObtenerPost();
  }, []);

  //Obtener Detalles - PETICIÓN GET + ID
  const obtenerDetalle = async (id) => {
    setDetalles(null);
    console.log(id);
    setId(id);
    try {
      const { data } = await axios.get(
        ` https://jsonplaceholder.typicode.com/posts/${id}`
      );
      console.log(data);
      setDetalles(data);
    } catch (err) {
      console.error(err);
    }
  };
  // Eliminar usuario- PETICIÓN DELETE
  const deleteUser = (id) => {
    setUsers(users.filter((user) => user.id !== id));
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "DELETE",
    });
  };

  // Función Agregar Usuario
  const addUser = (user) => {
    console.log(user);
    setNewPost(user);
    setUsers([...users, user]);
    console.log(users);
    llamadoPost();
  };
  // PETICIÓN POST PARA AGREGAR DATOS
  function llamadoPost() {
    fetch("https://jsonplaceholder.typicode.com/posts", {
      method: "POST",
      body: JSON.stringify({
        title: newPost.title,
        body: newPost.body,
        userId: newPost.id,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => console.log(json, "Nuevo llamada Post hacia la api "));
  }

  // Editar usuario
  const [editing, setEditing] = useState(false);
  const [currentUser, setCurrentUser] = useState({
    id: null,
    title: "",
    body: "",
  });

  //Funcion para recibir datos usuario a modificar y cambia a formulario edit
  // El parametro user, puede tener cualquier nombre se refiere a la "x" que mapea antes los datos
  const editRow = (user) => {
    setEditing(true);
    setCurrentUser({ id: user.id, title: user.title, body: user.body });
    console.log(user.id);
  };
  //Actualiza la información
  const updateUser = (id, update) => {
    setEditing(false);
    setUsers(users.map((user) => (user.id === id ? update : user)));
    console.log(updateUser);
    console.log(users);
    console.log(update);
    ModificarDatosApi(id, update);
  };

  //PETICIÓN PUT PARA MODIFICAR DATOS
  // Le paso el parametro update con los input actualizados en la función de arriba
  function ModificarDatosApi(id, update) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        id: id,
        title: update.title,
        body: update.body,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) =>
        console.log(json, "Datos modificados para subir a la api")
      );
  }

  // USE MEMO, optipiza
  const Titles = useMemo(() => {
    return (
      users &&
      users.map((x) => (
        <TitleList
          deleteUser={deleteUser}
          key={x.id}
          title={x.title}
          id={x.id}
          obtenerDetalle={obtenerDetalle}
          detalles={detalles}
          editRow={editRow}
          x={x}
        />
      ))
    );
  }, [users]);
  return (
    <React.Fragment>
      <Heading />
      <div className="text-center mt-5">
        <div className="container ">
          <div className="row  ">
            <div className="col-md-6">
              {editing ? (
                <Edit currentUser={currentUser} updateUser={updateUser} />
              ) : (
                <Add addUser={addUser} />
              )}
            </div>
            <div className="col-md-6  border-left">
              <h3 className="mb-5">Titles</h3>
              {Titles}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default withRouter(Home);
