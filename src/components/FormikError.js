import React from "react";
import { ExclamationTriangleFill } from "react-bootstrap-icons";

function FormikError(props) {
  return (
    <div className="border rounded mt-3 text-danger p-1 align-items-center d-flex justify-content-center ">
      <ExclamationTriangleFill /> <span className="ml-1" /> {props.children}
    </div>
  );
}

export default FormikError;
