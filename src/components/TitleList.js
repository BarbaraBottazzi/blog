import React from "react";
import { PencilFill, TrashFill } from "react-bootstrap-icons";
import DetailAlert from "./DetailAlert";

function TitleList({
  x,
  id,
  title,
  obtenerDetalle,
  editRow,
  deleteUser,
  detalles,
}) {
  return (
    
    <div>
      <React.Fragment key={id}>
        
        <p className="m-4">{title}</p>

        <DetailAlert
          id={id}
          obtenerDetalle={obtenerDetalle}
          detalles={detalles}
        />
        <button
          className="btn btn-warning m-3"
          onClick={() => {
            editRow(x);
          }}
        >
          <PencilFill />{" "}
        </button>
        <button
          onClick={() => {
            deleteUser(id);
          }}
          className="btn btn-danger"
        >
          <TrashFill />
        </button>
      </React.Fragment>
    </div>
  );
}

export default TitleList;
