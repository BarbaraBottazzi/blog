import React from "react";
import { useState } from "react";
import { PatchCheck, PlusSquareFill } from "react-bootstrap-icons";
import { Formik, ErrorMessage, Field } from "formik";
import { v4 as uuidv4 } from "uuid";
import * as Yup from "yup";
import FormikError from "./FormikError";
import { Alert } from "react-bootstrap";

function Add({ addUser }) {
  const [alerta, setAlerta] = useState(false);

  const validate = Yup.object({
    title: Yup.string()
      .max(15, "Must be 15 characters or less")
      .required("Required"),
    body: Yup.string()
      .max(30, "Must be 30 characters or less")
      .required("Required"),
  });
  return (
    <div className="container">
      {/* condicion se puede en  otro componente o usememo(buscar) */}
      {alerta ? (
        <Alert variant="success">
          Añadido correctamente <span className="ml-2" /> <PatchCheck />
        </Alert>
      ) : null}

      <Formik
        initialValues={{
          title: "",
          body: "",
        }}
        validationSchema={validate}
        onSubmit={(values) => {
          values.id = uuidv4();
          console.log(values);
          addUser(values);
          setAlerta(true);
          console.log(alerta);
          setTimeout(() => {
            setAlerta(false);
          }, 1000);
        }}
      >
        {(formik) => (
          <div className="row d-flex justify-content-center">
            <div className="text-center">
              <h3 className="mb-3">Add Titles</h3>

              <form onSubmit={formik.handleSubmit}>
                <Field
                  placeholder="Title.."
                  type="text"
                  name="title"
                  className=" form-control rounded"
                />
                <ErrorMessage name="title" component={FormikError} />
                <br />
                <div>
                  <label htmlFor="body"></label>
                  <Field
                    placeholder="Body.."
                    className="mt-3 form-control rounded"
                    type="text"
                    name="body"
                  />
                  <ErrorMessage name="body" component={FormikError} />
                </div>

                <br />
                <button
                  type="submit"
                  className="btn btn-primary-outline m-3"
                  type="submit"
                >
                  <PlusSquareFill color="green" size="2rem" value="submit" />
                </button>
              </form>
            </div>
          </div>
        )}
      </Formik>
    </div>
  );
}

export default Add;
