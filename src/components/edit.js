import React from "react";
import { PencilFill } from "react-bootstrap-icons";
import { Formik, ErrorMessage, Field } from "formik";
import * as Yup from "yup";
import FormikError from "./FormikError";

function Edit({ currentUser, updateUser }) {
  const validate = Yup.object({
    title: Yup.string()
      .max(10, "Must be 10 characters or less")
      .required("Required"),
    body: Yup.string()
      .max(30, "Must be 30 characters or less")
      .required("Required"),
  });

  return (
    <div className="container">
      <Formik
        initialValues={{
          title: currentUser.title,
          body: currentUser.body,
          id: currentUser.id,
        }}
        validationSchema={validate}
        onSubmit={(values) => {
          console.log(values);
          values.id = currentUser.id;
          updateUser(currentUser.id, values);
        }}
      >
        {(formik) => (
          <div className="row d-flex justify-content-center">
            <div className="text-center">
              <h3 className="mb-3">Edit Titles</h3>
              <form onSubmit={formik.handleSubmit}>
                <Field
                  placeholder="Title.."
                  type="text"
                  name="title"
                  className=" form-control rounded"
                />
                <ErrorMessage name="title" component={FormikError} />
                <br />
                <div>
                  <label htmlFor="body"></label>
                  <Field
                    placeholder="Body.."
                    className="mt-3 form-control rounded"
                    type="text"
                    name="body"
                  />
                  <ErrorMessage name="body" component={FormikError} />
                </div>

                <br />
                <button
                  type="submit"
                  className="btn btn-primary-outline m-3"
                  type="submit"
                >
                  <PencilFill color="green" size="1.5rem" />{" "}
                </button>
              </form>
            </div>
          </div>
        )}
      </Formik>
    </div>
  );
}

export default Edit;
