import React, { useState } from "react";
import { Alert, Button } from "react-bootstrap";

function DetailAlert({ id, obtenerDetalle, detalles }) {
  const [show, setShow] = useState(false);

  return (
    <>
      <Alert show={show} variant="info">
        <b>Title:</b>
        <p>{(detalles && detalles.title) || `No existen los datos`}</p>
        <b>Body: </b>
        <p>{(detalles && detalles.body) || `No existen los datos`}</p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="primary">
            Close
          </Button>
        </div>
      </Alert>

      {!show && (
        // para multiple funciones separas con ;
        <Button
          onClick={() => {
            setShow(true);
            obtenerDetalle(id);
          }}
        >
          Detalles
        </Button>
      )}
    </>
  );
}

export default DetailAlert;
